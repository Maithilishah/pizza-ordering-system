/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import static java.lang.Double.parseDouble;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import static javafx.scene.control.SelectionMode.MULTIPLE;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.Pizza;
import model.PizzaOptions;


/**
 * FXML Controller class
 *
 * @author Maithili
 */
public class FXMLOrderPizzaController implements Initializable {

    @FXML
    private ImageView imgTakeaway;
    @FXML
    private ImageView imgDelivery;
    @FXML
    private Label lblTakeAway, lblDelivery, lblPreview;
    @FXML
    private ComboBox<String> cmbsize;
    @FXML
    private ListView<String> lstSauces;
    @FXML
    private Button btnStart;
    @FXML
    private Object grpCrust;
    @FXML
    private ToggleGroup grpchoosecrust, grpsize;
    @FXML
    private RadioButton optHand, optThin;
    @FXML
    private ComboBox optsmall, optmed, optlarge, optparty, optextlarge;
    @FXML
    private CheckBox chkMushroom, chkOnion, chkTomato, chkPepper, chkOlive, chkCheese, chkSpanich, chkJalapeno;
    @FXML
    private TextArea txtComments;
    @FXML
    private TextField nameField, addField, phoneField ;
    
    String[] listSize = {"SMALL", "Medium", "Large", "Extra large", "Party"};
    String[] listSauces = {"Pizza Sauce", "Barbeque Sauce", "Alfredo Sauce", "Ranch Sauce", "Tartar Sauce", "Garlic Sauce", "Mayonnaise"};
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> obsSize = FXCollections.observableArrayList(listSize);
        cmbsize.setItems(obsSize);
        cmbsize.getSelectionModel().selectFirst();
        
        ObservableList<String> obsSauceList = FXCollections.observableArrayList(listSauces);
        lstSauces.setItems(obsSauceList);
        lstSauces.getSelectionModel().setSelectionMode(MULTIPLE);
        
        lstSauces.setOnMouseClicked(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                pizza.setSauces(lstSauces.getSelectionModel().getSelectedItems());
                
            }
            
        });
    }    
    
    
    Pizza pizza = new Pizza();
    
 @FXML
    private void swapDelivery(MouseEvent event) {
        if (pizza.getDelivery() == 1) {
            pizza.setDelivery(0);
            imgDelivery.setImage(new Image(getClass().getResource("Delivery.jpg").toExternalForm()));
            lblDelivery.setText("Delivery");
        } else {
            pizza.setDelivery(1);
            imgDelivery.setImage(new Image(getClass().getResource("Smiley.jpg").toExternalForm()));
            lblDelivery.setText("Delivery Chosen");
        }
    }
    
 @FXML
    private void swapTakeaway(MouseEvent event) {
        if (pizza.getTakeaway() == 1) {
            pizza.setTakeaway(0);
            imgTakeaway.setImage(new Image(getClass().getResource("Takeaway.jpg").toExternalForm()));
            lblTakeAway.setText("Take Away");
        } else {
            pizza.setTakeaway(1);
            imgTakeaway.setImage(new Image(getClass().getResource("Smiley.jpg").toExternalForm()));
            lblTakeAway.setText("Take Away Chosen");
        }
    }
	
@FXML
    private void close(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                "Are you sure you wish to exit?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Exit Program");
        alert.setHeaderText(null);
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.YES) {
            System.exit(0);
        }
    }
    
    @FXML
    private void chooseCrust(ActionEvent event) {
        if (grpchoosecrust.getSelectedToggle() == optHand) {
            pizza.setCrust("Hand Tossed");
        } else if (grpchoosecrust.getSelectedToggle() == optThin) {
            pizza.setCrust("Thin Crust");
        } else {
            pizza.setCrust("Pan Crust");
        }
    }
    
    @FXML
    private void chooseSize(ActionEvent event){
        if (cmbsize.getValue() == "Small"){
            pizza.setSize("Small");
        }
        else if (cmbsize.getValue() == "Medium"){
            pizza.setSize("Medium");
        }
        else if (cmbsize.getValue() == "Large"){
            pizza.setSize("Large");
        }
        else if (cmbsize.getValue() == "Extra Large"){
            pizza.setSize("Extra Large");
        }
        else {
            pizza.setSize("Party");
        }
        
        
    }
    
    
    @FXML
    private void chooseSauce(ActionEvent event){
        
    }
    
    
    @FXML
    private void namefield(ActionEvent event){
        //nameField.getText();
        pizza.setName(nameField.getText().toString());
    }
    
    @FXML
    private void addfield(ActionEvent event){
        addField.setText("Address");
    }
    
    @FXML
    private void phonefield(ActionEvent event){
        phoneField.setText("Phone");
    }
  
    @FXML 
    private void chooseMushroom(ActionEvent event) {
        if (chkMushroom.isSelected()) {
             pizza.toppings.add("Mushroom");
        }
    }
    @FXML
    private void chooseOnion(ActionEvent event) {
        if (chkOnion.isSelected()) {
              pizza.toppings.add("Onion");
        }
    }
    @FXML
    private void chooseTomato(ActionEvent event) {
        if (chkTomato.isSelected()) {
              pizza.toppings.add("Tomato");
        }
    }
    @FXML
    private void choosePepper(ActionEvent event) {
        if (chkPepper.isSelected()) {
              pizza.toppings.add("Pepper");
        }
    }
    @FXML
    private void chooseOlive(ActionEvent event) {
        if (chkOlive.isSelected()) {
              pizza.toppings.add("Olive");
        }
    }
    @FXML
    private void chooseCheese(ActionEvent event) {
        if (chkCheese.isSelected()) {
              pizza.toppings.add("Extra Cheese");
        }
    }
    @FXML
    private void chooseSpanich(ActionEvent event) {
        if (chkSpanich.isSelected()) {
            pizza.toppings.add("Spanich");
        }
    }
    @FXML
    private void chooseJalapeno(ActionEvent event) {
        if (chkJalapeno.isSelected()) {
              pizza.toppings.add("Jalapeno");
        }
    }
    
    @FXML
    private void chooseSauces(ActionEvent event) {
        
    }
    @FXML
    private void placeOrder(ActionEvent event) {
     lblPreview.setText("Free Order has placed successfully!!!!");
     pizza.setName(nameField.getText().toString());
     pizza.setAddress(addField.getText().toString());
     pizza.setPhone(phoneField.getText().toString());
        txtComments.setText(pizza.toString());
    }
    
    @FXML
    private void startOver(ActionEvent event){
        nameField.setText("Name");
        phoneField.setText("Phone");
        addField.setText("Address");
        cmbsize.getSelectionModel().selectFirst();
        optThin.setSelected(false);
        optHand.setSelected(false);
        chkMushroom.setSelected(false);
        chkOlive.setSelected(false);
        chkOnion.setSelected(false);
        chkCheese.setSelected(false);
        chkTomato.setSelected(false);
        chkSpanich.setSelected(false);
        chkPepper.setSelected(false);
        chkJalapeno.setSelected(false);
        txtComments.setText("");
        lblPreview.setText("Preview Order");
        
    }
        
    }
       
        

