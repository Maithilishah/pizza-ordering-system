/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Calendar;
import java.util.Date;
import javafx.collections.ObservableList;

/**
 *
 * @author Maithili
 */
public class Pizza {
    public String size;
    public String crust;
    public PizzaOptions method;
    public String name;
    public String phone;
    public String address;
    public int orderId;
    public int Takeaway;
    public int Delivery;
    
    public List<String> toppings = new ArrayList<String>()
    {{

    }};
    public List<String> sauces = new ArrayList<String>()
    {{
      
    }};

    
    public Pizza(){
    }

    public List<String> getToppings() {
        return toppings;
    }

    public void setToppings(List<String> toppings) {
        this.toppings = toppings;
    }

    public List<String> getSauces() {
        return sauces;
    }

    public void setSauces(List<String> sauces) {
        this.sauces = sauces;
    }
    
    public Pizza(PizzaOptions sauce) {
        this.sauces = sauces;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCrust() {
        return crust;
    }

    public void setCrust(String crust) {
        this.crust = crust;
    }

    public PizzaOptions getMethod() {
        return method;
    }

    public void setMethod(PizzaOptions method) {
        this.method = method;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getTakeaway() {
        return Takeaway;
    }

    public void setTakeaway(int Takeaway) {
        this.Takeaway = Takeaway;
    }

    public int getDelivery() {
        return Delivery;
    }

    public void setDelivery(int Delivery) {
        this.Delivery = Delivery;
    }
    

    public void setToppings(String mushroom) {
        this.toppings=toppings;
    }
    
        Calendar date = Calendar.getInstance();
        int timeInSecs = 0;
        Date deliveryTime = new Date(30 * 60 * 1000);

    @Override
    public String toString() {
        return "Order ID: #11896" + "\n" + "Order Placed At: " + date.getTime() + "\n" + "Estimated time of delivery: " + deliveryTime + "\n" + "Method: Delivery" + "\n" + "Crust: "+ crust + "\n" + "Size: " + size + "\n" + "Toppings: " + toppings.toString() + "\n"  + "Sauces: " + sauces.toString() + "\n" + "Name: " + name + "\n" + "Phone: "+ phone + "\n" + "Address: " + address;
    }

    
}
