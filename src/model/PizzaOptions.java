/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Maithili
 */
public enum PizzaOptions{

    MUSHROOM("Mushroom"), 
    ONION("Onion"),
    TOMATO("Tomato"),
    PEPPER("Pepper"),
    OLIVE("Olive"),
    EXTRA_CHEESE("Extra Cheese"),
    SPINACH("Spinach"),
    JALAPENO("Jalapeno"),
    HOME_DELIVERY("Home Delivery"),
    TAKE_AWAY("Take Away"),
    HAND_TOSSED("Hand Tossed"),
    THIN_CRUST("Thin Crust"),
    PAN_CRUST("Pan Crust"),
    SMALL("Small"),
    MEDIUM("Medium"),
    LARGE("Large"),
    EXTRA_LARGE("Extra Large"),
    PARTY("Party"),
    PIZZA_SAUCE("Pizza Sauce"),
    BBQ_SAUCE("Barbeque Sauce"),
    ALFREDO("Alfredo"),
    GARLIC_SAUCE("Garlic Sauce"),
    RANCH("Ranch Sauce"),
    TARTAR("Tartar Sauce"),
    MAYO("Mayonnaise");
    
private String displayname;

    public String getDisplayname() {
        return displayname;
    }

PizzaOptions(String name){
     this.displayname = displayname;
}

    @Override
    public String toString() {
        return "PizzaOptions{" + "displayname=" + this.displayname + '}';
    }



}
